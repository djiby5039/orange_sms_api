import httplib2
import urllib
import json
 
http = httplib2.Http()
 
body = {'grant_type': 'client_credentials'}

def getToken():
    content = http.request("https://api.orange.com/oauth/v3/token", 
                       method="POST", 
                       headers={'Content-type': 'application/x-www-form-urlencoded','Authorization':  'Basic MjBtUWVPNnRTWXZ0TlN4S0E1TzNneUVjVWF1UWxNZ0I6Y3V6dkV4eTdqNGRuWk5uMg=='},
                       body=urllib.parse.urlencode(body))[1]
 
    #print(json.loads(content.decode())['access_token'])
    orangesms_token=json.loads(content.decode())['access_token']
    return orangesms_token
    
def getSms(telephone):
    orangesms_body = {"outboundSMSMessageRequest":{"address": "tel:"+telephone,"outboundSMSTextMessage": {"message": "Bonjour Sika sy "},"senderAddress": "tel:"+telephone,"senderName": "Python-Flask"}}
    orangesms_token =getToken()
    getSms_result = http.request("https://api.orange.com/smsmessaging/v1/outbound/tel:+221773387902/requests", 
                       method="POST", 
                       headers={'Content-type': 'application/json','Authorization':  'Bearer '+orangesms_token},
                       body= json.dumps(orangesms_body))[1]
                       #body=urllib.parse.urlencode(body))[1]
                       #body= orangesms_body)
                                
    return getSms_result

# print(getSms("+221773387902"))

def getBalance():
    orangesms_token =getToken()
    get_balance = http.request("https://api.orange.com/sms/admin/v1/contracts", 
                       method="GET", 
                       headers={'Content-type': 'application/json','Authorization':  'Bearer '+orangesms_token})

    return get_balance
                       
print(getBalance()) 
