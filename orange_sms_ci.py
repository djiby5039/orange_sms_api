import httplib2
import urllib
import json
import datetime
import re

http = httplib2.Http()
 
body = {'grant_type': 'client_credentials'}

## Token d'authorisation
def getToken(authorization):
    content = http.request("https://api.orange.com/oauth/v3/token", 
                       method="POST", 
                       headers={'Content-type': 'application/x-www-form-urlencoded','Authorization':  authorization},
                       body=urllib.parse.urlencode(body))[1]
 
    #print(json.loads(content.decode())['access_token'])
    orangesms_token=json.loads(content.decode())['access_token']
    return orangesms_token

## Envoi de SMS    
def sendSms(apiTelNumber,senderTel,receiverTel,smsMsg,authorization,senderName="SMS 673090"):
    orangesms_body = {"outboundSMSMessageRequest":{"address": "tel:"+receiverTel,"outboundSMSTextMessage": {"message": smsMsg},"senderAddress": "tel:"+senderTel,"senderName": senderName}}
    orangesms_token =getToken(authorization)
    # example of api tel number used is 2250777017381 without + or 00
    getSms_result = http.request("https://api.orange.com/smsmessaging/v1/outbound/tel:+"+apiTelNumber+"/requests", 
                       method="POST", 
                       headers={'Content-type': 'application/json','Authorization':  'Bearer '+orangesms_token},
                       body= json.dumps(orangesms_body))[1]
                       #body=urllib.parse.urlencode(body))[1]
                       #body= orangesms_body)
                                
    return getSms_result

## Envoi de SMS a une base de numero dans un fichier texte
def sendSmsIppsBase(authorization,message):
    date = datetime.datetime.now()
    logName = "envoi_sms_IPPSbase_logs_{}.txt".format(date.strftime("%Y%m%d-%H%M%S")) 
    log = open(logName, "w")
    #logs = ""
    
    balancedebut = getBalanceUnits(authorization)
    time = datetime.datetime.now()
    line = "{} | balance {} | debut envoi".format(time.strftime("%Y-%m-%d %H:%M:%S"),balancedebut)
    #logs += line
    log.write(line+"\r\n")
    log.flush()

    f = open("ippsbase.txt", "r")
    for x in f:
        res = re.search("\d{9}",x)
        if res != None and len(x.strip()) == 9:
            time = datetime.datetime.now()
            num = x.strip()
            envoi = sendSms("221773387902","+221773387902","+221"+num,message,james_authorization)
            line = "{} | numéro : {} | resultat envoi : {}".format(time.strftime("%Y-%m-%d %H:%M:%S"), num, envoi)
            #print(line)
            #logs += line
            log.write(line+"\r\n")
            log.flush()
    f.close()

    balancefin = getBalanceUnits(authorization)
    line = "{} | balance {} | fin envoi".format(time.strftime("%Y-%m-%d %H:%M:%S"),balancefin)
    #logs += line
    log.write(line+"\r\n")
    log.flush()
    
    log.close()

    return "Fin Envoi"

## Envoi de SMS a une base de numero dans un fichier texte
def sendSmsIppsBase2(authorization,message):

    orangesms_token =getToken(authorization)
    
    date = datetime.datetime.now()
    logName = "{}_envoi_sms_RPN_logs.txt".format(date.strftime("%Y%m%d-%H%M%S")) 
    log = open(logName, "w")
    #logs = ""
    ## Ligne debut de log
    balancedebut = getBalanceUnits(authorization)
    time = datetime.datetime.now()
    line = "{} | balance {} | debut envoi".format(time.strftime("%Y-%m-%d %H:%M:%S"),balancedebut)
    #logs += line
    log.write(line+"\r\n")
    log.flush()
    ## Ouvrir fichier base
    f = open("ippsbase.txt", "r")
    for x in f:
        res = re.search("\d{9}",x)
        if res != None and len(x.strip()) == 9:
            time = datetime.datetime.now()
            num = x.strip()
            ## ENVOI SMS
            #envoi = sendSms("221773387902","+221773387902","+221"+num,message,james_authorization)
            orangesms_body = {"outboundSMSMessageRequest":{"address": "tel:+221"+num,"outboundSMSTextMessage": {"message": message},"senderAddress": "tel:+221773387902","senderName": "ipps"}}
            getSms_result = http.request("https://api.orange.com/smsmessaging/v1/outbound/tel:+221773387902"+"/requests", 
                       method="POST", 
                       headers={'Content-type': 'application/json','Authorization':  'Bearer '+orangesms_token},
                       body= json.dumps(orangesms_body))[1]
            ## Ligne de log
            line = "{} | numéro : {} | resultat envoi : {}".format(time.strftime("%Y-%m-%d %H:%M:%S"), num, getSms_result)
            #print(line)
            #logs += line
            ## Ecriture de log
            log.write(line+"\r\n")
            log.flush()
    ## Fermer fichier base
    f.close()

    balancefin = getBalanceUnits(authorization)
    line = "{} | balance {} | fin envoi".format(time.strftime("%Y-%m-%d %H:%M:%S"),balancefin)
    #logs += line
    log.write(line+"\r\n")
    log.flush()
    
    log.close()

    return "Fin Envoi"


## Envoi de SMS a une base de numero dans un fichier texte
def sendSmsBase(authorization,fichier,message):

    orangesms_token =getToken(authorization)
    
    date = datetime.datetime.now()
    logName = "logs/{}_envoi_sms_IPPSbase_logs.txt".format(date.strftime("%Y%m%d-%H%M%S")) 
    log = open(logName, "w")
    #logs = ""
    ## Ligne debut de log
    balancedebut = getBalanceUnits(authorization)
    time = datetime.datetime.now()
    line = "{} | balance {} | debut envoi".format(time.strftime("%Y-%m-%d %H:%M:%S"),balancedebut)
    #logs += line
    log.write(line+"\r\n")
    log.flush()
    ## Ouvrir fichier base
    f = open(fichier, "r")
    for x in f:
        res = re.search("\d{9}",x)
        if res != None and len(x.strip()) == 9:
            time = datetime.datetime.now()
            num = x.strip()
            ## ENVOI SMS
            #envoi = sendSms("221773387902","+221773387902","+221"+num,message,james_authorization)
            orangesms_body = {"outboundSMSMessageRequest":{"address": "tel:+225"+num,"outboundSMSTextMessage": {"message": message},"senderAddress": "tel:+221773387902","senderName": "ipps"}}
            getSms_result = http.request("https://api.orange.com/smsmessaging/v1/outbound/tel:+221773387902"+"/requests", 
                       method="POST", 
                       headers={'Content-type': 'application/json','Authorization':  'Bearer '+orangesms_token},
                       body= json.dumps(orangesms_body))[1]
            ## Ligne de log
            line = "{} | numéro : {} | resultat envoi : {}".format(time.strftime("%Y-%m-%d %H:%M:%S"), num, getSms_result)
            #print(line)
            #logs += line
            ## Ecriture de log
            log.write(line+"\r\n")
            log.flush()
    ## Fermer fichier base
    f.close()

    balancefin = getBalanceUnits(authorization)
    line = "{} | balance {} | fin envoi".format(time.strftime("%Y-%m-%d %H:%M:%S"),balancefin)
    #logs += line
    log.write(line+"\r\n")
    log.flush()
    
    log.close()

    return "Fin Envoi"

## Solde de comptes SMS
def getBalance(authorization):
    orangesms_token =getToken(authorization)
    header,get_balance = http.request("https://api.orange.com/sms/admin/v1/contracts", 
                       method="GET", 
                       headers={'Content-type': 'application/json','Authorization':  'Bearer '+orangesms_token})

    return get_balance

## Bellow function return current balance units
def getBalanceUnits(authorization):
   return json.loads(getBalance(authorization).decode())[0]['availableUnits']
    # return json.loads(getBalance(authorization).decode())['partnerContracts']['contracts'][0]['serviceContracts'][0]['availableUnits'] 

djiby_authorization= 'Basic cW9UdjVJN2VOYkJvN1JMbWh6aXN1Nk90Unp5UlJJS2c6TEpoZzczYldrSnNyTk1HSA'
james_authorization= 'Basic MjBtUWVPNnRTWXZ0TlN4S0E1TzNneUVjVWF1UWxNZ0I6Y3V6dkV4eTdqNGRuWk5uMg=='
sdb_authorization= 'Basic R2tHOG5kcXRHTnFNUUhiaERnVXRJQWtIWlg5R3llb0g6QXVrdTNrZzZFZGNZcGhJaA=='
# print("La balance SMS du compte de djiby est : {}".format(getBalanceUnits(djiby_authorization)))
# print("La balance SMS du compte de james est : {}".format(getBalanceUnits(james_authorization)))
# print(json.loads(getBalance().decode())['partnerContracts']['contracts'][0]['serviceContracts'][0]['availableUnits'])
# print("envoi sms du compte de james, statut : {}".format(sendSms("221773387902","+221773387902","+221765005555","[IPPS] Shalom, nous reprenons nos programmes du 16 au 18 Avr. 2021 à 18h à l'imprimerie Monteiro avec Elie Diatta. Plus d'infos ici : https://bit.ly/3dZDH9B",james_authorization)))
# print(getSms("+2250777017381"))
# print(sendSmsIppsBase2(james_authorization,"[IPPS] Shalom, nous reprenons nos programmes du 16 au 18 Avr. 2021 à 18h à l'imprimerie Monteiro avec Elie Diatta. Plus d'infos ici : https://bit.ly/3dZDH9B"))
# print(sendSmsBase(james_authorization,"chfamembres.txt","[CHFA] Bonjour team de valeur, nous avons remis 250.000F pour les programmes et 100.000F à Papa par la grâce de Dieu. Plus d'infos ici : https://bit.ly/3wV02Ol"))
# print(sendSmsBase(james_authorization,"sdb-3eanneecommunnion.txt","[CATESDB] Chers parents, vous êtes attendus ce samedi de 15h à 17h au secrétariat de la catéchèse pour retirer le bulletin de votre enfant en classe de 3eme année communion. les sacrements approchent et nous sommes à une étape décisive, ce sera l'occasion d'échanger avec vous sur l’éligibilité de votre enfant au sacrement. A ce samedi 17 avril.Présence obligatoire. Le bureau de la catéchèse"))
# print(sendSmsBase(james_authorization,"sdb-bureau.txt","[CATESDB] Chers parents, vous êtes attendus ce samedi de 15h à 17h au secrétariat de la catéchèse pour retirer le bulletin de votre enfant en classe de 3eme année communion. les sacrements approchent et nous sommes à une étape décisive, ce sera l'occasion d'échanger avec vous sur l’éligibilité de votre enfant au sacrement. A ce samedi 17 avril.Présence obligatoire. Le bureau de la catéchèse"))
# print(sendSmsBase(james_authorization,"sdb-comitebaptemeadulte2021.txt","[CATESDB] Cher parent/tuteur, le catéchumène Raphaël Moustapha Mbaye est convoqué ce samedi 17 avril à 9h par le père Cyprien concenrnant la démarche du baptême chrétien. Présence obligatoire. Le Bureau de la catéchèse. Contacter M. Niox au besoin 76 500 55 55"))
# print(sendSmsBase(james_authorization,"ippsbase.txt","Shalom famille, nous nous retrouverons du 23 au 25 Avr. 2021 à 18h à l'imprimerie Monteiro avec Elie Diatta. Thème :« Ils ont vu ton étoile… …mais qui ? » Matthieu 2. Plus d'infos ici : https://bit.ly/3dZDH9B"))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","Test sender name",james_authorization,"SMS 673090")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","Test sender name",sdb_authorization,"donboscodkr")))
# print(sendSmsBase(sdb_authorization,"sdb3eacommunion.txt","Chers parents de 3eme année communion, vous êtes invités à venir retirer les bulletins de vos enfants cet après-midi à la permanence de la catéchèse (bureau en bois), de 15h00 à 17h00. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"testsdbcate.txt","[TEST] Chers parents de 3eme année communion, vous êtes invités à venir retirer les bulletins de vos enfants cet après-midi à la permanence de la catéchèse, de 15h00 à 17h00. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"sdbcatebase.txt","Chers parents, exceptés les catechumènes du dimanche, les carnets de vos enfants sont disponibles. vous êtes invités à venir les récupérer aux heures de catéchèse et profiter de l'occasion pour échanger avec le catéchiste. Plus d'informations ici : https://bit.ly/2QS991V"))
# print(sendSmsBase(sdb_authorization,"bases/20210424_annoncerelance.txt","Chers parents, exceptés les catechumènes du dimanche, les carnets de vos enfants sont disponibles. vous êtes invités à venir les récupérer aux heures de catéchèse et profiter de l'occasion pour échanger avec le catéchiste. Plus d'informations ici : https://bit.ly/2QS991V"))
# print(sendSmsBase(sdb_authorization,"bases/20210424_annoncerelance_2.txt","Chers parents, exceptés les catechumènes du dimanche, les carnets de vos enfants sont disponibles. vous êtes invités à venir les récupérer aux heures de catéchèse et profiter de l'occasion pour échanger avec le catéchiste. Plus d'informations ici : https://bit.ly/2QS991V"))
# print(sendSmsBase(sdb_authorization,"bases/20210424_annoncerelance_3.txt","Chers parents, exceptés les catechumènes du dimanche, les carnets de vos enfants sont disponibles. vous êtes invités à venir les récupérer aux heures de catéchèse et profiter de l'occasion pour échanger avec le catéchiste. Plus d'informations ici : https://bit.ly/2QS991V"))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents de 3ème année communion, ce samedi 1er mai, votre enfant aura sa 3ème et dernière évaluation aux horaires habitules de cours(15h), leur présence est donc obligatoire. leur faire révisier les leçons du chapitre 09 au chapitre 15, ils s'agit de toutes les leçons relatives à l'eucharistie. Merci pour votre collaboration. le bureau de la catéchèse."))
# print(sendSmsBase(james_authorization,"bases/ippsbaseplus.txt","Shalom famille, Visitation Prophétique du 30 Avr. au 02 Mai 2021 à 18h à l'imprimerie Monteiro avec Elie Diatta. Plus d'infos ici : https://bit.ly/3dZDH9B"))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, Nous vous informons qu'il y a effectivement catéchèse ce samedi 1er mai 2021 et ce dimanche 02 mai (adultes) nous comptons sur vous pour la présence effective au cours. Nous comptons sur vous pour la présence des enfants/catéchumènes. Plus d'informations ici : https://bit.ly/2QS991V"))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents, vous êtes convoqués ce samedi 8 mai à la dernière réunion de préparation au sacrement pour votre enfant. Votre présence est obligatoire faute de quoi votre enfant ne sera pas admis au sacrement. Merci pour votre compréhension. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents, vous êtes convoqués ce samedi 8 mai à 17h à la dernière réunion de préparation au sacrement pour votre enfant. Votre présence est obligatoire faute de quoi votre enfant ne sera pas admis au sacrement. Merci pour votre compréhension. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/20210506_rejeuSdb.txt","Chers parents, vous êtes convoqués ce samedi 8 mai à 17h à la dernière réunion de préparation au sacrement pour votre enfant. Votre présence est obligatoire faute de quoi votre enfant ne sera pas admis au sa le bureau de la catéchèse."))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","Test \n a la ligne <br> ou comme ca",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","Chers parents,\nDates importantes pour la préparation à la communion :\n> récollection\n- jeu. 13 mai 09h-16h30\n- dim. 23 mai 09h-16h30\n- lun. 24 mai 09h-17h00\n> 1ère confession :\n- ven. 28 Mai à 16h\nNous rappelons que leur présence est obligatoire à ces dates sans dérogation possible.\dernier délai :\nDroit d’étole (5.000 F CFA) -> 13 mai\nExtraits de baptême -> 14 mai.\nMerci\nLe bureau",sdb_authorization,"donboscodkr")))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents,\nDates importantes pour la préparation à la communion :\n> récollection\n- jeu. 13 mai 09h-16h30\n- dim. 23 mai 09h-16h30\n- lun. 24 mai 09h-17h00\n> 1ère confession :\n- ven. 28 Mai à 16h\nNous rappelons que leur présence est obligatoire à ces dates sans dérogation possible.\dernier délai :\nDroit d’étole (5.000 F CFA) -> 13 mai\nExtraits de baptême -> 14 mai.\nMerci\nLe bureau"))
# print(sendSmsBase(sdb_authorization,"bases/20210511_rejeuSdb.txt","Chers parents,\nDates importantes pour la préparation à la communion :\n> récollection\n- jeu. 13 mai 09h-16h30\n- dim. 23 mai 09h-16h30\n- lun. 24 mai 09h-17h00\n> 1ère confession :\n- ven. 28 Mai à 16h\nNous rappelons que leur présence est obligatoire à ces dates sans dérogation possible.\dernier délai :\nDroit d’étole (5.000 F CFA) -> 13 mai\nExtraits de baptême -> 14 mai.\nMerci\nLe bureau"))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catéchistes, \nIl y a recollection des communiants : \n- jeudi 13 mai 09h00 - 16h30 \n- dimanche 23 mai 09h00 - 16h30 \n- lundi 24 mai 09h00 - 17h00 \nPrière de vous manifester pour que nous vous comptions parmi les animateurs(trices).\nMerci\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/20210511_rejeuSdb_catechistes.txt","Chers catéchistes, \nIl y a recollection des communiants : \n- jeudi 13 mai 09h00 - 16h30 \n- dimanche 23 mai 09h00 - 16h30 \n- lundi 24 mai 09h00 - 17h00 \nPrière de vous manifester pour que nous vous comptions parmi les animateurs(trices).\nMerci\nLe bureau de la catéchèse."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","Shalom Famille, soyez bénis.\nA l'occasion du 10ème Anniversaire de notre Ministère IPPS, nous recevrons deux Hautes autorités spirituelles qui seront nos invités durant tout le temps des programmes du mois de Septembre. (les dates vous seront communiquées ultérieurement) Vues toutes les dépenses qui nous incombent pour la préparation, l'organisation et l'après programmes,..."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","... le comité d'organisation a décidé que tous membres cotisent  50 000frcs au minimum pour pouvoir faire y faire face. Que chacun comprenne la dimension de cette activité qui aura un impact très positif dans nos vies. Nos autorités sauront compter sur l'engagement de tous, de façon individuelle d'abord, puis collective pour la bonne réussite de ce Grand événement. ..."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","... Date limite des cotisations : 31 JUILLET 2021. Vous pouvez les envoyer à Rose ou Vicky par Orange Money en précisant vos noms. Possibilité de cotiser en 3 mois... Merci. \nBerger NIXON"))
# print(sendSmsBase(james_authorization,"bases/ippsmembres_aspirants.txt","Shalom Famille, soyez bénis.\nA l'occasion du 10ème Anniversaire de notre Ministère IPPS, nous recevrons deux Hautes autorités spirituelles qui seront nos invités durant tout le temps des programmes du mois de Septembre. (les dates vous seront communiquées ultérieurement) Vues toutes les dépenses qui nous incombent pour la préparation, l'organisation et l'après programmes,..."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres_aspirants.txt","... le comité d'organisation a décidé que tous membres cotisent  50 000frcs au minimum pour pouvoir faire y faire face. Que chacun comprenne la dimension de cette activité qui aura un impact très positif dans nos vies. Nos autorités sauront compter sur l'engagement de tous, de façon individuelle d'abord, puis collective pour la bonne réussite de ce Grand événement. ..."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres_aspirants.txt","... Date limite des cotisations : 31 JUILLET 2021. Vous pouvez les envoyer à Rose ou Vicky par Orange Money en précisant vos noms. Possibilité de cotiser en 3 mois... Merci. \nBerger NIXON"))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents,\nne pas oublier le repas froid pour votre enfant, pour la récollection. \nLe Bureau de la Catéchèse"))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","Chers parents, vous êtes convoqués ce samedi 15 mai à 17h à la dernière réunion de préparation au sacrement de confirmation pour votre enfant. \nVotre présence est obligatoire faute de quoi votre enfant ne sera pas admis au sacrement. Merci pour votre compréhension. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb-bureau.txt","Chers parents, vous êtes convoqués ce samedi 15 mai à 17h à la dernière réunion de préparation au sacrement de confirmation pour votre enfant. \nVotre présence est obligatoire faute de quoi votre enfant ne sera pas admis au sacrement. Merci pour votre compréhension. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation_rejeu.txt","Chers parents, vous êtes convoqués ce samedi 15 mai à 17h à la dernière réunion de préparation au sacrement de confirmation pour votre enfant. \nVotre présence est obligatoire faute de quoi votre enfant ne sera pas admis au sacrement. Merci pour votre compréhension. le bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","Cher parents,\nMerci pour votre presence ce jour. Apres concertation avec le curé, les aubes seront finalement retenues comme l’année dernière.\nVous pourrez en louer avec \"Arbre divin\" au 76 733 67 98 ou 77 257 85 96 ou 77 463 05 17, veuillez nous excuser pour ce changement.\nQue les absents à la réunion se rapprochent du curé."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents,\nNous vous rappelons la nécessité de fournir l'extrait de baptême pour le sacrement de communion de votre enfant.\nPour ceux qui ne l'ont pas encore remis le dernier recours est pour ce samedi 22mai.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","[INFORMATION]\nChers parents,\nPour rappel les aubes seront de mise pour la confirmation pour les filles comme pour les garçons.\n\nMme NDiaye de notre paroisse en loue, elle est joignable au 338206938 et au 775138587.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents,\nComme annoncé, les photos et vidéos dans l'église seront exclusivement gérées par la catéchèse. Vous pouvez vous inscrire en suivant ce lien pour la communion et verser d'avance le prix des photos/vidéos : https://bit.ly/3hHMKQ4\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","Chers parents,\nComme annoncé, les photos et vidéos dans l'église seront exclusivement gérées par la catéchèse. Vous pouvez vous inscrire en suivant ce lien pour la confirmation et verser d'avance le prix des photos/vidéos : https://bit.ly/2RB7Xk3\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"logs/envoi_sms_IPPSbase_logs_20210521-121822_REJEU.txt","Chers parents,\nComme annoncé, les photos et vidéos dans l'église seront exclusivement gérées par la catéchèse. Vous pouvez vous inscrire en suivant ce lien pour la confirmation et verser d'avance le prix des photos/vidéos : https://bit.ly/2RB7Xk3\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","Chers parents,\nJuste pour vous informer que vos enfants sont encore à l'église pour les derniers ajustements pour samedi prochain. Merci pour votre compréhension.\nLe bureau de la catéchèse."))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221779646193","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221778170857","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221772753360","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221783361123","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221776556519","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221775212029","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221776130535","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221773387902","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\nDu f
# ait des sacrements de communion et de confirmation ce week-end, il n'y aura pas catéchèse.\n\nLe bureau de la catéchèse."))
# Aloise Marcel Sedar Sarr 221775327533
# Albert Raoul Faye 221775452777
# René Pierre Ndiaye 221775484330
##print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221773387902","Cher parent,\n l'enfant Aloise Marcel Sedar Sarr ne s'est pas encore acquité de son droit d'étole. Vous avez encore jusqu'à demain 18h00 pour le faire au niveau du curé.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
##print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221775327533","Cher parent,\n l'enfant Aloise Marcel Sedar Sarr ne s'est pas encore acquité de son droit d'étole. Vous avez encore jusqu'à demain 18h00 pour le faire au niveau du curé.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
##print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221775452777","Cher parent,\n l'enfant Albert Raoul Faye ne s'est pas encore acquité de son droit d'étole. Vous avez encore jusqu'à demain 18h00 pour le faire au niveau du curé.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
##print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221775484330","Cher parent,\n l'enfant René Pierre Ndiaye ne s'est pas encore acquité de son droit d'étole. Vous avez encore jusqu'à demain 18h00 pour le faire au niveau du curé.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
##print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221778590854","Cher parent,\n l'enfant René Pierre Ndiaye ne s'est pas encore acquité de son droit d'étole. Vous avez encore jusqu'à demain 18h00 pour le faire au niveau du curé.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation_rappelEB.txt","Chers parents,\nVous avez encore jusqu'à demain 18h00 pour déposer l'extrait de bapteme de votre enfant au niveau du curé, délai de rigueur.\nNous rappelons que c'est obligatoire pour le sacrement.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse"))
# 221774581844
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221774581844","Chers parents,\nComme annoncé, les photos et vidéos dans l'église seront exclusivement gérées par la catéchèse. Vous pouvez vous inscrire en suivant ce lien pour la communion et verser d'avance le prix des photos/vidéos : https://bit.ly/3hHMKQ4\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221774126591","Chers parents,\nComme annoncé, les photos et vidéos dans l'église seront exclusivement gérées par la catéchèse. Vous pouvez vous inscrire en suivant ce lien pour la communion et verser d'avance le prix des photos/vidéos : https://bit.ly/3hHMKQ4\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221774126591","Cher parent,\nNous avons tous les élémennts du dossier de Raphael excepté l'extrait de naissance, vous pouvez envoyer la photo à M. Niox (76 500 55 55) par whatsapp ou l'amener au plus tôt à la par\nMeri pour votre compréhension.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents,\nNous vous rappelons la confession de votre enfant demain à partir de 16h. Elle est obligatoire.\nLes horaires des sacrements :\n- Confirmations à 16h le samedi 29 mai\n- Communions à 11h le dimanche 30 mai\nLes enfants sont convoqués 30min avant.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","Chers parents,\nNous vous rappelons la confession de votre enfant demain à partir de 16h. Elle est obligatoire.\nLes horaires des sacrements :\n- Confirmations à 16h le samedi 29 mai\n- Communions à 11h le dimanche 30 mai\nLes enfants sont convoqués 30min avant.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/20210527_comconf_retry.txt","Chers parents,\nNous vous rappelons la confession de votre enfant demain à partir de 16h. Elle est obligatoire.\nLes horaires des sacrements :\n- Confirmations à 16h le samedi 29 mai\n- Communions à 11h le dimanche 30 mai\nLes enfants sont convoqués 30min avant.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Il y aura la dernière répétition après les confessions.\n les enfants finiront un peu plus tard que d'habitude.\nMerci par avance pour votre compréhension.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdb3eanneecofirmation.txt","Il y aura la dernière répétition après les confessions.\n les enfants finiront un peu plus tard que d'habitude.\nMerci par avance pour votre compréhension.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Shalom famille, nous nous retrouverons du 04 au 06 Juin 2021 à 18h à l'imprimerie Monteiro avec Elie Diatta et papa Lago. Thème :« Connais-tu la grâce ? » Plus d'infos ici : https://bit.ly/3dZDH9B"))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","Chers membres CHFA,\n\nNous vous rappelons les cotisations mensuelles de 10.000 F CFA pour la caisse specialement en cette veille de programme. Comme à l'accoutumée vous pouvez vous rapprocher de la soeur Modjidine Ndong au 77 656 14 66. Dieu nous bénisse.\n\n Le Bureau CHFA.\n\nVos feedbacks ici : https://bit.ly/3chFzL3",james_authorization,"ipps")))
# print(sendSmsBase(james_authorization,"bases/chfamembres.txt","Chers membres CHFA,\n\nNous vous rappelons les cotisations mensuelles de 10.000 F CFA pour la caisse specialement en cette veille de programme. Comme à l'accoutumée vous pouvez vous rapprocher de la soeur Modjidine Ndong au 77 656 14 66. Dieu nous bénisse.\n\n Le Bureau CHFA.\n\nVos feedbacks ici : https://bit.ly/3chFzL3"))
# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Shalom famille,\nAprès avoir rencontré la grâce, nous allons faire sa connaissance, et elle va bouleverser nos vies.\nIl ne faut pas manquer ce rendez-vous je t’invite moi l'esclave de JÉSUS , Elie Diatta Bikamoi.\nQue la grâce vous localise !!!\n\nPlus d'infos ici : https://bit.ly/3dZDH9B"))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221773387902","Chers parents, les catéchistes seront en formation du doyenné ce samedi 12 juin 2021.\nLes cours vaqueront donc ce samedi, mais pour les catéchumènes du dimanche ils sont maintenus.\nLes cours reprendrons le samedi 19 juin. Il y aura la dernniere evaluation le samedi 26 juin et la cloture de la catéchèse sera pour le samedi 03 juillet 2021.\n\nle bureau de la catéchèse",sdb_authorization,"donboscodkr")))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, les catéchistes seront en formation du doyenné ce samedi 12 juin 2021.\nLes cours vaqueront donc ce samedi, mais pour les catéchumènes du dimanche ils sont maintenus.\nLes cours reprendrons le samedi 19 juin. Il y aura la dernière évaluation le samedi 26 juin et la cloture de la catéchèse sera pour le samedi 03 juillet 2021.\n\nle bureau de la catéchèse"))
# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Shalom famille,\nAprès avoir rencontré la grâce, nous allons faire sa connaissance, et elle va bouleverser nos vies.\nIl ne faut pas manquer ce rendez-vous je t’invite moi l'esclave de JÉSUS , Elie Diatta Bikamoi.\nQue la grâce vous localise !!!\n\nPlus d'infos ici : https://bit.ly/3dZDH9B"))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers Catechistes,\nNous vous présentons nos excuses pour le manque de communication, suite au dernier message envoyé aux parents du fait de la formation du samedi 12 juin passé, les dates à retenir sont :\n- dernier contrôle le 26 juin\n- Clôture de la catéchèse le 03 juillet.\nMerci pour votre compréhension\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes_sansciblecotisation.txt","Chers catéchistes,\nPour rester fidèles à notre engagement de solidarité pour nos soeurs et frères dans la douleur ou dans un heureux évènement, nous vous sollicitons pour une collecte, montant à votre discretion, pour un geste symbolique pour chaqu'un d'eux avant la fin de la catéchèse.\nA envoyer à Mme Bocandé au 77 645 89 71.\nQue Dieu vous bénisse.\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes_sansciblecotisation.txt",".. les contributions sont à faire au plus tard avant le samedi 26 juin 2021.\nQue Dieu vous bénisse.\n\nLe bureau de la catéchèse."))

# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","[Berger NIXON] Shalom famille soyez bénis. Je veux juste rappeler à un frère et à une sœur que nous nous approchons de la date limite des cotisations pour l'organisation du 10ème anniversaire de IPPS. Le temps n'étant plus notre allié, j'invite tous les membres à faire un effort pour donner au moins une avance car bientôt la fin du mois. Passez un excellent week-end sous la protection divine."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres_aspirants.txt","[Berger NIXON] Shalom famille soyez bénis. Je veux juste rappeler à un frère et à une sœur que nous nous approchons de la date limite des cotisations pour l'organisation du 10ème anniversaire de IPPS. Le temps n'étant plus notre allié, j'invite tous les membres à faire un effort pour donner au moins une avance car bientôt la fin du mois. Passez un excellent week-end sous la protection divine."))
# 776567420
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221776567420","[Berger NIXON] Shalom famille soyez bénis. Je veux juste rappeler à un frère et à une sœur que nous nous approchons de la date limite des cotisations pour l'organisation du 10ème anniversaire de IPPS. Le temps n'étant plus notre allié, j'invite tous les membres à faire un effort pour donner au moins une avance car bientôt la fin du mois. Passez un excellent week-end sous la protection divine.",james_authorization,"ipps")))

#print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\nil y a bel et bien cours ce matin, le dernier controle est primordial pour la cloture de la catachese et le passage des enfants à ue etape supérieure. Nous comptons sur votre collaboration et compréhension.\nMerci.\n\nLe bureau de la catéchèse."))
#print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catéchistes,\nil y a bel et bien cours ce matin, le dernier controle est primordial pour la cloture de la catachese et le passage des enfants à ue etape supérieure. Nous comptons sur votre collaboration et compréhension.\nMerci.\n\nLe bureau de la catéchèse."))
#770520301
#print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221770520301","Chers parents,\nil y a bel et bien cours ce matin, le dernier controle est primordial pour la cloture de la catachese et le passage des enfants à ue etape supérieure. Nous comptons sur votre collaboration et compréhension.\nMerci.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
#776546643
#print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221776546643","Chers parents,\nil y a bel et bien cours ce matin, le dernier controle est primordial pour la cloture de la catachese et le passage des enfants à ue etape supérieure. Nous comptons sur votre collaboration et compréhension.\nMerci.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))

#776533581
#print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221776533581","Chers catéchistes,\nil y a bel et bien cours ce matin, le dernier controle est primordial pour la cloture de la catachese et le passage des enfants à ue etape supérieure. Nous comptons sur votre collaboration et compréhension.\nMerci.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
#784237451
#print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221784237451","Chers catéchistes,\nil y a bel et bien cours ce matin, le dernier controle est primordial pour la cloture de la catachese et le passage des enfants à ue etape supérieure. Nous comptons sur votre collaboration et compréhension.\nMerci.\n\nLe bureau de la catéchèse.",sdb_authorization,"donboscodkr")))
#print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\npour ceux qui sont venus a 8h et repartis, nous sommes sur place. Vous pouvez ramener votre enfant, nous souhaitons éviter tout report de la clôture de la catéchèse.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse."))
#20210626_retry.txt
# print(sendSmsBase(sdb_authorization,"logs/20210626_retry.txt","Chers parents,\npour ceux qui sont venus a 8h et repartis, nous sommes sur place. Vous pouvez ramener votre enfant, nous souhaitons éviter tout report de la clôture de la catéchèse.\nMerci pour votre compréhension.\n\nLe bureau de la catéchèse."))
#776546643
# print(sendSmsBase(sdb_authorization,"bases/sdb3eacommunion.txt","Chers parents, le controôle ne concerne pas les classes de sacrement quand bien même les enfants sont attendus ce jour. Merci\n\nLe bureau de la catéchèse."))
# print(sendSmsBase(sdb_authorization,"bases/sdbcate_rejeu20210630.txt","Chers parents,\nla catéchèse prend fin ce sam. 03/07/2021 avec un goûter partager. Les enfants doivent venir avec des jus et buscuits comme participation. Pour les absents du samedi passé, un devoir de rattrappage est prévu. Les bulletins seront disponibles aux heures de cours. Nous comptons sur votre collaboration pour une clôture en beauté. Que Dieu vous bénisse.\n\nLe bureau de la catéchèse."))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catéchistes, nous aurons notre messe de clôture ce dimanche 04 juillet à 11h suivie de notre assemblée du bilan de l'année catéchistique et d'une collation. Vue l'importance de l'ordre du jour votre présence à tous est vivement souhaitée. Que Dieu vous bénisse.\n\nLe bureau de la catéchèse.\n\nNB:Le gouter partagé du samedi se fera par classe du fait du COVID. Merci"))

# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","[Berger NIXON] Shalom famille soyez bénis. Nous ouvrons une collecte pour soutenir notre soeur Catherine Ndiaye du groupe des chantres à l'occasion du rappel à Dieu de sa mère. Que chacun donne ce qu'il peut selon son coeur. Veillez envoyer vos cotisations au 77 410 47 00. Dernier délai, vedredi 02/07/21 Que Dieu vous le rende au centuple."))
# print(sendSmsBase(james_authorization,"bases/ippsmembres_aspirants.txt","[Berger NIXON] Shalom famille soyez bénis. Nous ouvrons une collecte pour soutenir notre soeur Catherine Ndiaye du groupe des chantres à l'occasion du rappel à Dieu de sa mère. Que chacun donne ce qu'il peut selon son coeur. Veillez envoyer vos cotisations au 77 410 47 00. Dernier délai, vedredi 02/07/21 Que Dieu vous le rende au centuple."))

# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","[Berger NIXON]\nShalom famille soyez bénis.\nRencontre ce samedi 17/07 de 17H à 18H30 au siège (NORD FOIRE après le terminus TATA 34)\nOrdre du jour :\n1° rappeler et développer quelques points sur l’organisation des 10 ans de IPPS\n2° La vie dans le ministère\n3° Quelques Orientations pour les nouveaux membres\n4° Questions diverses\nConvocation à 16H30 - fin 18H30\n\nPRÉSENCE OBLIGATOIRE."))
# 775179857
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"775179857","[Berger NIXON]\nShalom famille soyez bénis.\nRencontre ce samedi 17/07 de 17H à 18H30 au siège (NORD FOIRE après le terminus TATA 34)\nOrdre du jour :\n1° rappeler et développer quelques points sur l’organisation des 10 ans de IPPS\n2° La vie dans le ministère\n3° Quelques Orientations pour les nouveaux membres\n4° Questions diverses\nConvocation à 16H30 - fin 18H30\n\nPRÉSENCE OBLIGATOIRE.",james_authorization,"ipps")))
# 770745364
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"770745364","[Berger NIXON]\nShalom famille soyez bénis.\nRencontre ce samedi 17/07 de 17H à 18H30 au siège (NORD FOIRE après le terminus TATA 34)\nOrdre du jour :\n1° rappeler et développer quelques points sur l’organisation des 10 ans de IPPS\n2° La vie dans le ministère\n3° Quelques Orientations pour les nouveaux membres\n4° Questions diverses\nConvocation à 16H30 - fin 18H30\n\nPRÉSENCE OBLIGATOIRE.",james_authorization,"ipps")))
# 771827907
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"771827907","[Berger NIXON]\nShalom famille soyez bénis.\nRencontre ce samedi 17/07 de 17H à 18H30 au siège (NORD FOIRE après le terminus TATA 34)\nOrdre du jour :\n1° rappeler et développer quelques points sur l’organisation des 10 ans de IPPS\n2° La vie dans le ministère\n3° Quelques Orientations pour les nouveaux membres\n4° Questions diverses\nConvocation à 16H30 - fin 18H30\n\nPRÉSENCE OBLIGATOIRE.",james_authorization,"ipps")))
# 772787249
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"772787249","[Berger NIXON]\nShalom famille soyez bénis.\nRencontre ce samedi 17/07 de 17H à 18H30 au siège (NORD FOIRE après le terminus TATA 34)\nOrdre du jour :\n1° rappeler et développer quelques points sur l’organisation des 10 ans de IPPS\n2° La vie dans le ministère\n3° Quelques Orientations pour les nouveaux membres\n4° Questions diverses\nConvocation à 16H30 - fin 18H30\n\nPRÉSENCE OBLIGATOIRE.",james_authorization,"ipps")))
# 770468214
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"770468214","[Berger NIXON]\nShalom famille soyez bénis.\nRencontre ce samedi 17/07 de 17H à 18H30 au siège (NORD FOIRE après le terminus TATA 34)\nOrdre du jour :\n1° rappeler et développer quelques points sur l’organisation des 10 ans de IPPS\n2° La vie dans le ministère\n3° Quelques Orientations pour les nouveaux membres\n4° Questions diverses\nConvocation à 16H30 - fin 18H30\n\nPRÉSENCE OBLIGATOIRE.",james_authorization,"ipps")))

# print(sendSmsBase(james_authorization,"bases/chfamembres.txt","Chers membres de la CHFA, nous recevons par la grâce de Dieu la PAMECAS dans le cadre de nos activités mensuelles le samedi 17/07 à 10h30.\n\nRendez-vous ce samedi donc à 9h chez Officier. Ce sera l’occasion de faire le point succinct mensuel et donner l’information sur l’organisation des 10 ans avant la rencontre avec PAMECAS.\n\nNous comptons sur tous pour une participation massive.\n\nLe Bureau"))

# 765005555
## print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"765005555","Shalom Tata Victoire\nLa famille Ipps te souhaite un joyeux anniversaire tres longue vie,nous prions que la  santé divine soit ton partage  et que les écluses des cieux s'ouvrent dans ta vie au nom de Jésus Christ",james_authorization,"ipps")))
# 775513089
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"775513089","Shalom Tata Victoire\nLa famille Ipps te souhaite un joyeux anniversaire tres longue vie,nous prions que la  santé divine soit ton partage  et que les écluses des cieux s'ouvrent dans ta vie au nom de Jésus Christ",james_authorization,"ipps")))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"785232548","Shalom Tata Victoire\nLa famille Ipps te souhaite un joyeux anniversaire tres longue vie,nous prions que la  santé divine soit ton partage  et que les écluses des cieux s'ouvrent dans ta vie au nom de Jésus Christ",james_authorization,"ipps")))

# print(sendSmsBase(james_authorization,"bases/ippsmembres_sanselodie.txt","[Berger NIXON] Shalom famille soyez bénis. Nous ouvrons une collecte pour soutenir notre soeur Elodie du groupe des chantres à l'occasion du rappel à Dieu de son beau père. Que chacun donne ce qu'il peut selon son coeur. Veillez envoyer vos cotisations au 77 410 47 00. Que Dieu vous le rende au centuple."))

# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","[Berger NIXON] Shalom famille soyez bénis.\n Un rappel concernant la cotisation pour l'organisation des 10 ans d'IPPS. Nous sommes à quelques jours de l'échéance et nous comptons sur chacun pour la réussite de cet événement unique.\n Passez une excellente fin de semaine sous la protection divine."))
# 2021contribSono.txt
# print(sendSmsBase(james_authorization,"bases/2021contribSono.txt","[Pr. Elie Diatta]\nShalom famille, soyez bénis. Je voudrais rencontrer tous ceux qui avaient pris l'engagement de soutenir l'oeuvre avec l'achat d'une nouvelle sono. Venez avec votre contribution que je prie pour vous si vous ne l'avez pas encore remise. La date de la rencontre vous sera communiquée tres prochainnement.\nQue Dieu vous bénisse.\n https://bit.ly/2VwFXQD"))
# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","[Berger NIXON] Shalom famille soyez bénis.\nJ’ai le plaisir et la joie de vous annoncer le mariage de notre sœur Virginie faye de l’accueil ce samedi 21 AOÛT 2021 à 11H à la mairie de SICAP MBAO. tout le monde est invité. Merci\n\nBerger Nixon"))

# print(sendSmsBase(james_authorization,"bases/ippsmembres.txt","[Berger NIXON] Shalom famille soyez bénis.\n Un rappel concernant la cotisation pour l'organisation des 10 ans d'IPPS. Nous avons déjà dépassé l'échéance et nous comptons toujours sur votre soutien la réussite de cet événement unique.\n Passez une excellente fin de semaine sous la protection divine."))

# print(sendSmsBase(james_authorization,"bases/chfamembres.txt","Chers membres CHFA,\n\nNous vous rappelons les cotisations mensuelles de 10.000 F CFA pour la caisse. Comme d'habitude vous pouvez vous rapprocher de la soeur Modjdine Ndong au 77 656 14 66.\n Prière de lui notifier votre envoi par SMS pour lui faciliter la tâche.\n Dieu nous bénisse.\n\n Le Bureau CHFA"))

# print(sendSmsBase(james_authorization,"bases/chfamembres.txt","Chers membres CHFA,\n\nNous vous rappelons également la contribution pour l'organisation des 10 ans d'IPPS. Nous arrivons à l'échéance et comptons sur votre soutien pour la réussite de cet événement.\nVous pouvez faire l'envoi à la soeur Modjdine Ndong au 77 656 14 66.\n Prière de lui notifier votre envoi par SMS pour lui faciliter la tâche.\n Dieu nous bénisse.\n\n Le Bureau CHFA"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-bureau.txt","[CATECHESE]\nShalom Shalom à tous,\n Rencontre avec le Curé le dimanche 05 Septembre après la messe de 9h.\nLa présence de tous est vivement souhaitée voir obligatoire\nMerci\nLe Président"))

# print("La balance SMS du compte de DON BOSCO est : {}".format(getBalanceUnits(sdb_authorization)))
### 13/09/2021 09:11 698
# print("La balance SMS du compte de IPPS est : {}".format(getBalanceUnits(james_authorization)))
### 13/09/2021 09:11 1280
##### INSCRIPTIONS CATE ANNONCE
#print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\nLes inscriptions à la catéchèse débutent ce samedi 18 Sept. et prendront fin le samedi 16 Oct. Les cours débuterons le 09 Oct. 2021 Pour les inscriptions venez avec l'exrtait de baptême et l'extrait de naissance.\nPour rappel, la catéchèse paroissiale est obligatoire et les nouveaux venus en classe sacrement ne seront pas acceptées\ninfos: https://bit.ly/3EmEsGm\nle bureau"))
#print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Tarifs:\n1e an. pre-cate(CI) 2000\n2e an. pre-cate(CP) 2000\n1e an. communion(CE1) 3500\n2e an. communion(CE2) 3500\n3e an. communion(CM1) 4000\n1e an. confirmation(CM2) 4000\n2e an. confirmation(6e) 4000\n3e an. confirmation(5e) 4500\n1e an. persévérance(4e) 5000\n2e an. persévérance(3e) 5000\n3e an. persévérance(2nd) 5000\n4e an. persévérance(1ere) 5000\nAdultes 5000\nhttps://bit.ly/3EmEsGm"))
#print(sendSmsBase(sdb_authorization,"bases/20210913_sdb_retry.txt","Tarifs:\n1e an. pre-cate(CI) 2000\n2e an. pre-cate(CP) 2000\n1e an. communion(CE1) 3500\n2e an. communion(CE2) 3500\n3e an. communion(CM1) 4000\n1e an. confirmation(CM2) 4000\n2e an. confirmation(6e) 4000\n3e an. confirmation(5e) 4500\n1e an. persévérance(4e) 5000\n2e an. persévérance(3e) 5000\n3e an. persévérance(2nd) 5000\n4e an. persévérance(1ere) 5000\nAdultes 5000\nhttps://bit.ly/3EmEsGm"))
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221"+"765005555","Nous pouvons tout par Jesus et pour Jesus. C'est Lui le Chemin, la Vérité, la Vie. Posons ensemble, dans sa présence un pas de plus vers notre destinée. Ne rate pas ce RV avec ta destinée et partage cette joie, invite un max de personnes. Rendez-vous les vendredi 17, samedi 18 et dimanche 19 sept. 2021 à l'imprimerie Monteiro à partir de 18h. PORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B",james_authorization,"ipps")))
#print(sendSmsBase(james_authorization,"bases/testbase.txt","Nous pouvons tout par Jesus et pour Jesus. C'est Lui le Chemin, la Vérité, la Vie. Posons ensemble, dans sa présence un pas de plus vers notre destinée. Ne rate pas ce RV avec ta destinée et partage cette joie, invite un max de personnes. Rendez-vous les vendredi 17, samedi 18 et dimanche 19 sept. 2021 à l'imprimerie Monteiro à 18h. PORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))
# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Nous pouvons tout par Jesus et pour Jesus. C'est Lui le Chemin, la Vérité, la Vie. Posons ensemble, dans sa présence un pas de plus vers notre destinée. Ne rate pas ce RV avec ta destinée et partage cette joie, invite un max de personnes. Rendez-vous les vendredi 17, samedi 18 et dimanche 19 sept. 2021 à l'imprimerie Monteiro à 18h. PORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))
# print(sendSmsBase(james_authorization,"bases/log_sms_20219016.txt","Nous pouvons tout par Jesus et pour Jesus. C'est Lui le Chemin, la Vérité, la Vie. Posons ensemble, dans sa présence un pas de plus vers notre destinée. Ne rate pas ce RV avec ta destinée et partage cette joie, invite un max de personnes. Rendez-vous les vendredi 17, samedi 18 et dimanche 19 sept. 2021 à l'imprimerie Monteiro à 18h. PORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))
# print("La balance SMS du compte de IPPS est : {}".format(getBalanceUnits(james_authorization)))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Allons au front !!! 'Autel conte autel', nous combattons les autels de nos familles et celles dressées contre nous et notre destinée. C'est une étape essentielle, ne manque pas cette occasion. Rendez-vous les 1,2 et 3 Oct. 2021 à YMCA partir de 18h ou en direct sur Facebook et Youtube.\nPORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catéchistes, vous êtes convoqués ce samedi 2 Octobre 2021 à 10h pour la rentrée de la catéchèse.\nCe sera l'occasion pour faire le dispatching des classes et répertorier le manque de livres du catéchiste.\nNous finirons à 13h au plus tard. Vue l'importance de l'ordre du jour votre présence est cruciale et obligatoire.\nLe bureau"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catechistes,\nNous aurons une formation le dimanche prochain 09 Octobre 2021 après la messe de 9H avec le Curé.\nPrière le mettre dans votre programme. La Présence de tous est obligatoire. Merci et Excellente Semaine à tous!!!\nLe bureau"))

# print(sendSmsBase(james_authorization,"bases/chfamembres.txt","Chers membres CHFA,\n\nNous vous rappelons les cotisations mensuelles de 10.000 F CFA pour la caisse. Comme d'habitude vous pouvez vous rapprocher de la soeur Modjdine Ndong au 77 656 14 66.\n Prière de lui notifier votre envoi par SMS pour lui faciliter la tâche.\n Dieu nous bénisse.\n\n Le Bureau CHFA"))

# print(sendSmsBase(sdb_authorization,"bases/cas_blaise.txt","Chers parents,\naprès analyse et concertation du bureau, il a été retenu que Blaise Marcel Diouf reprend la classe de 1ere Année Confirmation pour l'annnée 2021-2022.\nLe bureau"))
# print(sendSmsBase(sdb_authorization,"bases/774261252.txt","Chers parents,\naprès analyse et concertation du bureau, il a été retenu que Blaise Marcel Diouf reprend la classe de 1ere Année Confirmation pour l'annnée 2021-2022.\nLe bureau"))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","2 Chroniques 1:7 Pendant la nuit, Dieu apparut à Salomon et lui dit: Demande ce que tu veux que je te donne. Merci SEIGNEUR pour cette nuit gravement prophétique. Famille y’a urgence 🚨 prophétique hummmmmmm\nNuit des Oracles avec JÉSUS-CHRIST, le dimanche 31 oct. 2021 de 22h à 06h au Lycée Kennedy,\nj’y serai et toi?\nPORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-catechistes-communion.txt","Shalom.\nNous allons faire une rencontre avec le Président et la responsable des classes de niveau Communion ce Samedi à 17h à la paroisse.\nVotre présence à cette réunion est nécessaire pour la bonne marche des cours.\nMerci de votre compréhension.\nLe Bureau"))

# print(sendSmsBase(sdb_authorization,"bases/catechumenes-adultes.txt","Chers catéchumènes, ce dimanche aura lieu la messe d'envoi de la catéchèse durant l'office de 11h. De ce fait vous aurez exceptionnellement cours à 09h, et la messe à 11h.\nLe Bureau de la catéchèse\n\nDear catechumens, this Sunday will take place the mass of sending of the catechesis during the service of 11 AM. Therefore, you will have class at 9 AM, and mass at 11 AM\nThe Catechesis Office"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catéchistes\nDimanche 14 nov. 21, nous aurons la messe d’envoi des catéchistes à 11h.\nC’est la messe d’envoi au cours de laquelle le curé donne pouvoir au catéchiste d’enseigner la catéchèse sur sa paroisse.\nVu l’importance de cette célébration, la présence de tous est obligatoire.\n\nNb: nous prions chacun de nous de se mettre en blanc, signe de la joie dans la mission\nSoyez bénis"))

# sdb-catebase-2021.txt
# print(sendSmsBase(sdb_authorization,"bases/sdb-catebase-2021.txt","Chers parents,\nnous avons le plaisir de vous annoncer la reprise des mouvements scout et  guide pour compléter à nouveau la panoplie de choix de mouvements. Nous comptons sur vous pour que vos enfants intègrent un mouvement, cela fait partie intégrante de la catéchèse paroissiale et c'est obligatoire. Merci par avance pour votre collaboration.\nLe bureau de la catéchèse"))

# Chers catéchistes, nous sommes désolés de vous annoncer que la messe d'envoi est reportée par souci de programmation de notre curé. Nous vous informerons de la nouvelle date. Le bureau de la catéchèses.
# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers catéchistes, nous sommes désolés de vous annoncer que la messe d'envoi est reportée par souci de programmation avec notre curé. Nous vous informerons de la nouvelle date. Le bureau de la catéchèses.\nSoyez bénis"))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Shalom famille, après Paris cap sur Dakar pour 'Explosion De Gloire'. JY SERAI ET TOI? Rendez-vous les 24, et 25 Nov. à YMCA et le 26 Nov. au lycée Kennedy. PORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Chers parents,\n ce samedi 5 dec. se tiendra le premier contrôle trimestriel, nous comptons sur vous pour faire réviser votre enfant et surtout vous assurer de sa présence. Pour les adultes ce sera le dimanche 6 dec. Merci par avance pour votre collaboration.\n Le Bureau de la Catéchèse\nSoyez bénis"))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\n ce samedi 5 dec. se tiendra le premier contrôle trimestriel, nous comptons sur vous pour faire réviser votre enfant et surtout vous assurer de sa présence. Pour les adultes ce sera le dimanche 6 dec. Merci par avance pour votre collaboration.\n Le Bureau de la Catéchèse\nSoyez bénis"))

# 17/12/2021
# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\nCe samedi  aura lieu le goûter partagé pour le départ des vacances de Noël en même temps que la remise des carnets de notes.\nQue chaque enfant vienne avec quelque chose pour le gouter partagé, et nous vous attendons pour la remise des carnets de notes.\nLes cours reprendront le 08 janvier 2022. Merci.\n Le Bureau de la catéchèse\nSoyez bénis\nhttps://bit.ly/3F9cRIK"))

# 202011217-sms_parents_rejeu.txt
# print(sendSmsBase(sdb_authorization,"bases/202011217-sms_parents_rejeu.txt","Chers parents,\nCe samedi  aura lieu le goûter partagé pour le départ des vacances de Noël en même temps que la remise des carnets de notes.\nQue chaque enfant vienne avec quelque chose pour le gouter partagé, et nous vous attendons pour la remise des carnets de notes.\nLes cours reprendront le 08 janvier 2022. Merci.\n Le Bureau de la catéchèse\nSoyez bénis\nhttps://bit.ly/3F9cRIK"))


# print(sendSmsBase(james_authorization,"bases/chfa-sans-charlotte.txt","Shalom famille,\nCordiale relance pour la collecte a l'attention de notre sœur éplorée Charlotte. Nous comptons sur vous, pour rappel c'est 5000 f à envoyer à la sœur Modjdine Ndong au 77 656 14 66.\nPrière de lui notifier votre envoi par SMS pour lui faciliter la tâche.\nUn rappel egalement pour les autres cotisations (25000 f du 31/12, 10.000f du mois).\nDieu nous bénisse.\n\nLe Bureau CHFA"))

# 03/01/2022

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, les inscriptions au pèlerinage des enfats de Popenguine du 09 Jan. 2022 sont encore ouvertes jusqu'au jeudi 06 Jan. a la paroisse.\n Enfant : 3.500F. Nous sollicitons également des encadreurs. Encadreur :2.500F.\n Contacter frère Jean Eudes au +221 77 409 51 76. \n Bonne et heureuse année 2022.\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, nous rappelons que les carnets sont toujours disponibles auprès des catéchistes. Prière de venir les récupérer et profiter de l'occasion pour voir l'évolution de votre enfant en échangeant avec son catéchiste. Que Dieu vous bénisse.\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(james_authorization,"bases/testbase.txt","Shalom famille,\nIl est stratégique, dans notre vie spirituelle de comprendre le mystère des cycles.\nVotre serviteur Elie Diatta Bikamoi inspiré par le St Esprit sera votre orateur.\nNe manquez-pas ce rendez-vous prophétique à l'IMPRIMERIE MONTEIRO du 28 au 30 Janvier 2022 à partir de 18H.\nPORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))
# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Shalom famille,\nIl est stratégique, dans notre vie spirituelle de comprendre le mystère des cycles.\nVotre serviteur Elie Diatta Bikamoi inspiré par le St Esprit sera votre orateur.\nNe manquez-pas ce rendez-vous prophétique à l'IMPRIMERIE MONTEIRO du 28 au 30 Janvier 2022 à partir de 18H.\nPORT DE MASQUE OBLIGATOIRE.\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","”Le fer aiguise le fer”\nDeux onctions prophétiques pour une orientation nouvelle\nSi tu es de Mbour, ce programme est pour toi:4 fév. Hotel Coco Beach, 17H\n\nPuis Cap sur Dakar,programme spécial célibataires:5 et 6 fév. à Monteiro à 18h\nViens prophétiser la fin de ton célibat, car ”Il n'est pas bon que l'homme soit seul”\nTheme : ”Je lui ferai une aide semblable” Gn 2,18\nhttps://bit.ly/3dZDH9B"))
#print(sendSmsBase(james_authorization,"bases/testbase.txt","test"))

# print(getToken(james_authorization))
# RRA7m7KObBDGDmXvRwzTUlnITw1I
# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221765005555","”Le fer aiguise le fer”\nDeux onctions prophétiques pour une orientation nouvelle\nSi tu es de Mbour, ce programme est pour toi: 4 fév. Hotel Coco Beach, 17H\nPuis Cap sur Dakar,programme spécial célibataires:5 et 6 fév. à Monteiro à 18h\nViens prophétiser la fin de ton célibat, car ”Il n'est pas bon que l'homme soit seul”\nTheme : ”Je lui ferai une aide semblable” Gn 2,18\nhttps://bit.ly/3dZDH9B",james_authorization,"ipps")))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","Chers parents, les enfants de 3ème année communion auront un contrôle ce samedi 05 Février 2022. Les enfants doivent réviser toutes les leçons 5 à 8. Nous comptons sur vous pour les préparer.\n\nMerci\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\n Don Bosco accueille ce samedi 12 février 2022 la formation des catéchistes du doyenné. De ce de fait, il n'y aura pas catéchèse pour les enfants. Les cours reprendronts le 19 février.\nQue Dieu vous bénissee.\n\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# 20220211_sdb_replay
# print(sendSmsBase(sdb_authorization,"bases/20220211_sdb_replay.txt","Chers parents,\n Don Bosco accueille ce samedi 12 février 2022 la formation des catéchistes du doyenné. De ce de fait, il n'y aura pas catéchèse pour les enfants. Les cours reprendronts le 19 février.\nQue Dieu vous bénissee.\n\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","[3ème année communion]\nChers parents, comme il n'y a pas cours ce samedi, nous vous prions de faire faire comme devoir de maison, la leçon ”Comment se confesser” sur la feuille qu'on leur a distribué, dans le cas contraire se reporter à la leçon 8 du livre de catéchèse. Merci.\n\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))
# print(sendSmsBase(sdb_authorization,"bases/20220211_sdb_replay2.txt","Chers parents,\n Don Bosco accueille ce samedi 12 février 2022 la formation des catéchistes du doyenné. De ce de fait, il n'y aura pas catéchèse pour les enfants. Les cours reprendronts le 19 février.\nQue Dieu vous bénissee.\n\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))


# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","[3ème année communion]\nChers parents, les carnets du 2ème trimestre sont disponibles ce jour auprès des catéchistes. Vous êtes attendus. Une attention particulière est requise pour ceux qui n'ont pas la moyenne, notez que nous sommes dans le dernier virage.. Merci.\n\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(james_authorization,"bases/ippsbase_1200.txt","Shalom shalom. Un grand événement se prépare un programme à ne surtout pas manquer. Une seule date à retenir le lundi 4 avril au théâtre national Daniel Sorano. Entrée gratuite. Ne venez pas seul inviter vos proches et amis et ensemble on va crier Dieu pour notre nation avec IPPS et Elie Diatta Bikamoi à 15h.\nhttps://bit.ly/3dZDH9B"))
# print(sendSmsBase(james_authorization,"bases/ippsbase_683.txt","Shalom shalom. Un grand événement se prépare un programme à ne surtout pas manquer. Une seule date à retenir le lundi 4 avril au théâtre national Daniel Sorano. Entrée gratuite. Ne venez pas seul inviter vos proches et amis et ensemble on va crier Dieu pour notre nation avec IPPS et Elie Diatta Bikamoi à 15h.\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\n Don Bosco accueille ce samedi 12 février 2022 la formation des catéchistes du doyenné. De ce de fait, il n'y aura pas catéchèse pour les enfants. Les cours reprendronts le 19 février.\nQue Dieu vous bénissee.\n\n Le bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, \nles cours de la catechese  sont suspendus depuis ce samedi 9 avril pour reprendre le samedi 23 avril 2022. Sainte semaine à vous.\n Et Joyeuses Pâquees d'avance.\nLe bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","Chers parents de la classe de communion, vos enfants auront le dernier contrôle décisif pour l'éligibilité au sacrement le 23 Avril. Nous vous prions leur faire réviser les leçons des chapitres 9 à 18.  Sainte semaine à vous.\n Et Joyeuses Pâquees d'avance.\nLe bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, La classe de 3eme année confirmation aura devoir ce samedi 30 avril. La classe de 3eme année communion est attendue aux heures de cours. Pas de cours pour les autres classe jusqu'au samedi 6 mai 2022.\nLes parents des classes de sacrement sont convoqués ce samedi à 17h pour une réunion obligatoire en vue du sacrement.\nMerci.\nLe bureau de la catéchèse.\nhttps://bit.ly/3F9cRIK"))

# sdb-3eanneecofirmation.txt

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecofirmation.txt","Chers pareents,\nPour la classe de 3eme année Confirmation, la réunion des parents est prévue le samedi 7 mai 2022, au lieu du 30 avril.Pour rappel cette réunion est obligatoire.Merci.Le bureau de la catéchèse\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecofirmation.txt","Chers pareents,\nPour la classe de 3eme année Confirmation, la réunion des parents est prévue le samedi 7 mai 2022 comme précédemment annoncé. Pour rappel cette réunion est obligatoire.Merci.Le bureau de la catéchèse\nhttps://bit.ly/3F9cRIK"))

# sdb-baptemes2022.txt
# print(sendSmsBase(sdb_authorization,"bases/sdb-baptemes2022.txt","INFORMATIONS BAPTÊME :\n Les Étapes avant le Baptême (obligatoires)\nDimanche 15 Mai à 9h (1ere Étape et 2e Étape)\nChaque enfant fera sa demande officiellement devant la communauté en rédigeant une lettre. La Lettre sera lue par le curé et acceptera la demande (1ere Étape)\nEnsuite chaque enfant viendra avec une Bible (TOB OU BIBLE DE JÉRUSALEM)."))
# print(sendSmsBase(sdb_authorization,"bases/sdb-baptemes2022.txt","Dimanche 22 Mai à 11h : Exorcisme + huile des Catéchumènes\nSamedi 28 Mai à 10h : Célébration du Baptême\nHabillement couleur blanche\nNb: Les Parrains et Marraines sont priés d’assister aux différentes étapes.\nVeuillez svp amener l’extrait de naissance de chaque enfant ainsi que l’extrait de baptême de chaque parrain et marraine(Obligatoire).\n pour écouter l'annonce : https://bit.ly/39aYsjQ"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-baptemes2022.txt","Chers parents, nous vous rappelons que les extraits de baptême des parains et marraines sont attendus par le curé. Prière de les déposer au secretariat du curé ce samedi au plus tard. Merci par avance.\nLe bureau de la catéchèse."))

# formulaire  photos communion 2022
# https://bit.ly/3LJT8St


# formulaire  photos confirmation 2022
# https://bit.ly/3G2U42U

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecofirmation.txt","INFORMATIONS CONFIRMATION\n\nDroit d’étole : 6000 pour chaque enfant (dernier délai Jeudi 26 Mai)\n\nHabillement Garçon: Aube blanche avec cordon et croix, bien coiffé\n\nHabillement Fille: aube blanche avec croix et cordon, pas de maquillage, pas d’extravagance, pas de rouge à lèvres.\n\nRetraites: Samedi 7, 14, 21 aux heures de cours 15h-17h\n\nJeudi 26 Mai (jour de l’ascension, toute la journée…prévoir"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecofirmation.txt","un repas froid) convocation à 9h\n\nSamedi 28 Mai : Messe de Confirmation à 16h à St Pierre des Baobabs (Convocation à 14h à la paroisse) Aucun retard ne sera toléré\n\nLes 6000 du droit d’etole donnent droit au transport Aller-Retour\nTous les enfants partiront de la paroisse sans exception.\n\nNb: un enfant qui rate une séance de retraite fera sa confirmation l’année prochaine."))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecofirmation.txt","un enfant qui ne respectera pas  les consignes ci dessus, se fera retirer du groupe.\n*Prioriser le côté simple afin d’imprégner l’enfant dans la grâce de ce grand Sacrement*.\n\nLocation Aubes:\n- 775138587 Mme Ndiaye\n- 77 133 77 00/77 638 66 44 Mme Kantoussang\n(Les contacter vite afin de faire les essayages à temps)\n Pour les photos s'inscrire en suivant ce lien : https://bit.ly/3G2U42U"))


# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","INFORMATIONS COMMUNION\n\nDroit d’étole : 5000 pour chaque enfant (dernier délai : Jeudi 26 Mai)\n\nHabillement Garçon: Pantalon noir, chemise manche longue blanche noeud papillon noir bien coiffé\n\nHabillement Fille : pas de maquillage, couleur blanche pour la robe. Pas d’extravagance, pas de rouge à lèvres.Pas de traîne ni de gant\n\nRetraites : Samedi 7, 14, 21 aux heures de cours 15h-17h..."))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","Jeudi 26 Mai (jour de l’ascension, toute la journée…prévoir un repas froid) convocation à 9h\n\nVendredi 27 Mai : 1ere Confession à partir de 15h à la paroisse\n\nDimanche 29 Mai : Messe de Communion à 11h (Convocation à 9h45 à la paroisse)\n\nNb: un enfant qui rate une séance de retraite fera sa communion l’année prochaine..."))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","un enfant qui ne respectera pas  les consignes ci dessus,  se fera retirer du groupe.\n*Prioriser le côté simple afin d’imprégner l’enfant dans la grâce de ce grand Sacrement *.\n\n Pour les photos s'inscrire en suivant ce lien : https://bit.ly/3LJT8St\nLe bureau de la catéchèse."))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents,\nCompte tenu des sacrements ce week-end, il n'y aura pas cours de catéchèse ce samedi. \nLes communiants sont convoqués ce vendredi à 14h30 (rassemblement), les confessions commencent à 15h. Nous rappelons que c'est obligatoire pour ceux qui font les sacrements.\nLe Bureau.\nhttps://bit.ly/3F9cRIK"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, il n'y aura pas catéchèse le samedi 04 juin, la dernière évaluation est prévue pour le Samedi 11 juin. Photos et vidéos des sacrements, disponibles le 11 (ceux qui ont payé). Clôture de la catéchèse : dimanche 19 juin, Messe d'action de grâce à 11h00 suivie d'un yendou à la paroisse, participation par enfant 1.000 FCFA. Présence de tous obligatoire.\nLe Bureau."))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers parents, pour une bonne préparation de la journée d'amitié pour la clôture de la  catéchèse le 19 Juin, prière de remettre la participation de 1000 f cfa dès ce samedi 11 juin.\nMerci.\nLe Bureau."))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase.txt","Chers Parents, Juste vous rappeler que samedi y’aura pas cours mais à la place nous demandons aux enfants de participer massivement à la kermesse du collège. Nous rappelons les 1000F pour le dimanche 19 Juin pour clôturer en beauté l’année catéchètique 2021-2022. Merci\nPour les dépôts : Wave 765005555 / OM 773387902 de la cotisation, préciser le nom et la classe de l'enfant par SMS. Soyez bénis"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","Chers Parents,\nla fête du Saint-Sacrement(fête-Dieu) est une solennité.\nCeci devient une seconde fête pour les nouveaux communiés. \nUne procession aura lieu après la messe de 11h avec le St Sacrement dans le quartier.\nNous vous demandons de laisser vos enfants venir dans les habits de la 1ere Communion et dans une ambiance de joie et de"))

# print(sendSmsBase(sdb_authorization,"bases/sdb-3eanneecommunion.txt","gaieté, nous parcourerons le quartier avec des cris de Joie pour montrer la gloire de notre Dieu.\nPas de talons pour les filles.\nConvocation à 10h30 \nN’oublions pas d’envoyer les 1000frs pour le partage ce jour marquant la clôture de l’année."))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","shalom famille,\nQue Dieu vouss bénisse.\nNous démarrons un programme terrible!!!\nNotre invité de marque n'est tout autre que le RÉVÉREND BENNY DAG ANAAN, representant du ministère de DAG HEWARD-MILLS en COTE D'IVOIRE.\nIl y a des visitations qui nous arrivent rarement dans la vie, ne rate pas le passage de cette onction à Dakar.\n Elie Diatta Bikamoi\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(james_authorization,"bases/ippsbase_547.txt","shalom famille,\nQue Dieu vouss bénisse.\nNous démarrons un programme terrible!!!\nNotre invité de marque n'est tout autre que le RÉVÉREND BENNY DAG ANAAN, representant du ministère de DAG HEWARD-MILLS en COTE D'IVOIRE.\nIl y a des visitations qui nous arrivent rarement dans la vie, ne rate pas le passage de cette onction à Dakar.\n Elie Diatta Bikamoi\nhttps://bit.ly/3dZDH9B"))

# print(sendSmsBase(james_authorization,"bases/ippsbase_sansippschfa.txt","Shalom famille \n\nLes tissus pour la célébration de nos 10 ans prévue le 07 août au Grand théâtre sont disponibles en vente au 77 233 95 08 ou 76 285 00 51. C est le dress code pour un accès gratuit  au programme. \nPassez vos commandes dès maintenant car le stock est limité.\n\nLe Comité d'organisation Ipps"))

# print(sendSmsBase(james_authorization,"bases/ippsbase_sansippschfa_rejeu.txt","Shalom peuple de Dieu \n\nLes tissus pour la célébration de nos 10 ans prévue le 07 août au Grand théâtre sont disponibles en vente au 77 233 95 08 ou 76 285 00 51. C est le dress code pour un accès gratuit  au programme. \nPassez vos commandes dès maintenant car le stock est limité.\n\nLe Comité d'organisation Ipps"))

# print(sendSmsBase(james_authorization,"bases/ippsbaseOrange1400.txt","Shalom family!\nConvention internationale des IPP du 2 au 7 août à Dakar.\nThème:EBENEZER, Jusqu’ici tu m’as soutenu.\nAvec Pr. KONGO INÈS et Ap. JOSHUA TALENA.\n❤️2 au 5 - YMCA\n❤️6 Diner - Cercle Messe à 20h\n❤️7 - Grand Theatre à 15H\nDemeurez bénis ❤️\npartagez au max svp\nbit.ly/3dZDH9B"))

# print(sendSmsBase(james_authorization,"bases/ippsbaseOrange1400_rejeu20220719.txt","Shalom family!\nConvention internationale des IPP du 2 au 7 août à Dakar.\nThème:EBENEZER, Jusqu’ici tu m’as soutenu.\nAvec Pr. KONGO INÈS et Ap. JOSHUA TALENA.\n❤️2 au 5 - YMCA\n❤️6 Diner - Cercle Messe à 20h\n❤️7 - Grand Theatre à 15H\nDemeurez bénis ❤️\npartagez au max svp\nbit.ly/3dZDH9B"))

# ippsbase_orange1000
# print(sendSmsBase(james_authorization,"bases/ippsbase_orange1000.txt","Shalom famille\nCe weekend nous avons crié à Dieu pour sa faveur et il a repondu par le feu!!!\nRendez-vous vendredi 29 et samedi 30 Juillet a Monteiro pour crier pour notre pays !!!\nLouanges, Adoration, Intercession, Témoignages, Prophéties!!!\npartagez en masse svp.\nDemeurez bénis ❤️\nbit.ly/3dZDH9B"))
# print(sendSmsBase(sdb_authorization,"bases/sdbcatechistes.txt","Shalom Shalom à tous, \ncomme annoncé, nous avons nos sœurs catéchistes qui ont fait leur 1ere profession et notre frère Jean-Eudes qui quittera le Sénégal après 2 ans pour la suite de sa formation. Pour leur acheter un cadeau symbolique nous vous demandons une contribution peu importe. Vous pouvez contactez Marie Florence Mendy, trésorière au +221 77 444 41 53"))

# print(sendSmsBase(james_authorization,"bases/ippsbase_41_20220831.txt","Shalom famille! \nSpécial programme du 02 au 04 sept. 2022 à Monteiro. \nTheme: Les théraphim de Laban.\nNe manquez pas ce rendez-vous du prophétique, des délivrances, JÉSUS sera glorifié. Ne venez pas seul.\nDieu vous bénisse 🙏🏾❤️\nInfos:bit.ly/3dZDH9B\nStop les SMS: bit.ly/3vXGHx1"))

# print(sendSmsBase(james_authorization,"bases/ippsbase_120_20220831.txt","Shalom famille! \nSpécial programme du 02 au 04 sept. 2022 à Monteiro. \nTheme: Les théraphim de Laban.\nNe manquez pas ce rendez-vous du prophétique, des délivrances, JÉSUS sera glorifié. Ne venez pas seul.\nDieu vous bénisse 🙏🏾❤️\nInfos:bit.ly/3dZDH9B\nStop les SMS: bit.ly/3vXGHx1"))
# sdbcatebase2022-23.txt

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase2022-23.txt","Chers parents,\nNous rappelons que demain 05 novembre marquera la fin des inscriptions.\nLes cours ont débuté depuis deux samedis\nVotre enfant doit venir avec un cahier de 200p et un bic pour les leçons à copier.\nLes livres sont remis à l'inscription.\nMerci\nLe bureau"))

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase2022-23_samedi.txt","Chers parents,\nNous vous informons que le premier controle de catéchèse pour le premier trimestre se fera ce samedi 10 dec. 2022 aux heures habituelles de cours.\nNous comptons sur vous pour la présence obligatoire de votre enfant.\nLe Bureau de la catéchèse."))

# print(getBalanceUnits(sdb_authorization))

# print("statut SMS : {}".format(sendSms("221773387902","+221773387902","+221773387902","Chers parents,\nNous vous informons que le premier controle de catéchèse pour le premier trimestre se fera ce samedi 10 dec. 2022 aux heures habituelles de cours.\nNous comptons sur vous pour la présence obligatoire de votre enfant.\nLe Bureau de la catéchèse.",sdb_authorization,"donboscodkr")))

# sdbcatebase2022-23_samedi_rejeu20221208

# print(sendSmsBase(sdb_authorization,"bases/sdbcatebase2022-23_samedi_rejeu20221208.txt","Chers parents,\nNous vous informons que le premier controle de catéchèse pour le premier trimestre se fera ce samedi 10 dec. 2022 aux heures habituelles de cours.\nNous comptons sur vous pour la présence obligatoire de votre enfant.\nLe Bureau de la catéchèse."))

# print(sendSmsBase(james_authorization,"bases/765005555.txt","Pr 11,25 \"L’âme bienfaisante sera rassasiée, et celui qui arrose sera arrosé.\"\nRPN et IPPS organisent leur conférence à Dakar du 21 au 26 fév. 2023\nSoutenez ces ministères qui œuvrent pour les défavorisés, vos dons et offrandes par Wave/OM:778089020\nbit.ly/3dZDH9B"))

# restepartenariat20230127

# print(sendSmsBase(james_authorization,"bases/restepartenariat20230127.txt","Pr 11,25 \"L’âme bienfaisante sera rassasiée, et celui qui arrose sera arrosé.\"\nRPN et IPPS organisent leur conférence à Dakar du 21 au 26 fév. 2023\nSoutenez ces ministères qui œuvrent pour les défavorisés, vos dons et offrandes par Wave/OM:778089020\nbit.ly/3dZDH9B"))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","\"En vérité, en vérité, je vous le dis, celui qui croit en moi fera aussi les oeuvres que je fais, et il en fera de plus grandes…\" Jn 14,12\nIls ont le mandat pour relâcher la puissance de Jesus qui fait des miracles\nFamille IPPS et invitez massivement a cette grâce exceptionnelle de 3 onctions filiales réunies.\nRPN et IPPS organisent leur conférence à Dakar du 21 au 26 fév. 2023\nbit.ly/3dZDH9B"))

# print(sendSmsBase(james_authorization,"bases/ippsbase.txt","Nous avons semé par révélation de ce mystère en décembre 2022 et maintenant nous allons récolter en 2023. \nViens et vois un mystère et son accomplissement. Papa Elie Diatta Bikamoi va nous connecter au mystère des récoltes.\nNous fermerons « Ebenezer » pour ouvrir « KABOTH 2023 ».\nNe rate pas cette occasion, invite au max. Nous serons à YMCA du 15 au 17 février 2023 de 18h30 à 22h.\nbit.ly/3dZDH9B"))
print(sendSmsBase(james_authorization,"bases/rpnbase.txt","Nous avons semé par révélation de ce mystère en décembre 2022 et maintenant nous allons récolter en 2023. \nViens et vois un mystère et son accomplissement. Papa Elie Diatta Bikamoi va nous connecter au mystère des récoltes.\nNous fermerons « Ebenezer » pour ouvrir « KABOTH 2023 ».\nNe rate pas cette occasion, invite au max. Nous serons à YMCA du 15 au 17 février 2023 de 18h30 à 22h.\nbit.ly/3dZDH9B"))